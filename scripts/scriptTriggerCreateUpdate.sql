use db;

go
create table audit_budge(
project_no char(4) null,
user_name char(16) null,
date datetime null,
budget_old float null,
budgt_new float null
);

go
create trigger modify_budget
on dbo.project AFTER UPDATE
AS IF UPDATE(BUDGET)
BEGIN
DECLARE @BUDGET_OLD FLOAT
DECLARE @BUDGET_NEW FLOAT
DECLARE @PROJECT_NUMBER CHAR(4)
SELECT @BUDGET_OLD = (SELECT BUDGET FROM deleted)
SELECT @BUDGET_NEW = (SELECT BUDGET FROM inserted)
SELECT @PROJECT_NUMBER = (SELECT project_nro FROM inserted)
insert into audit_budge values 
(@PROJECT_NUMBER, USER_NAME(), GETDATE(), @BUDGET_OLD, @BUDGET_NEW)
END
