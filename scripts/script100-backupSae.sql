use sae;
BACKUP DATABASE sae
	TO DISK ='C:\backup\sae.bak'
	WITH INIT, COMPRESSION;