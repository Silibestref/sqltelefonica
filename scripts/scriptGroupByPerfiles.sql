use db
select per_descripcion, pan_descripcion
from perfiles as per			inner join perfiles_pantallas perpan
on per.PER_ID = perpan.PER_ID	inner join pantallas as pan
on perpan.pan_id = pan.pan_id


-- quiero saber cuantas pantallas itene cada perfil

select per_descripcion, count(per_descripcion) 'cantidad de pantallas'
from perfiles as per			inner join perfiles_pantallas perpan
on per.PER_ID = perpan.PER_ID	inner join pantallas as pan
on perpan.pan_id = pan.pan_id
group by PER_DESCRIPCION
-- having count(per_descripcion)<6


-- quiero saber cuantos perfiles tiene cada pantalla

select pan_descripcion, count(pan_descripcion) 'cantidad de perfiles'
from perfiles as per			inner join perfiles_pantallas perpan
on per.PER_ID = perpan.PER_ID	inner join pantallas as pan
on perpan.pan_id = pan.pan_id
group by PAN_DESCRIPCION
