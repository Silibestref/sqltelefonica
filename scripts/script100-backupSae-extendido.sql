use sae;

-- practica de backup- extendidos
  insert into alumnos(part_id, com_id, ALU_NOMBRE, ALU_APELLIDO) values
  (1,1, 'Gabriel', 'Casas');

  -- Crea un bacup diferencial
  backup database sae
   to DISK = 'C:\backup\sae_diferencial.dif'
  WITH INIT, COMPRESSION, DIFFERENTIAL


  -- agregar 3 alumnos mas
  use sae;
   insert into alumnos(part_id, com_id, ALU_NOMBRE, ALU_APELLIDO) values
  (1,1, 'Mengano', 'Mengano');
   insert into alumnos(part_id, com_id, ALU_NOMBRE, ALU_APELLIDO) values
  (1,1, 'Fulano', 'Fulano');
   insert into alumnos(part_id, com_id, ALU_NOMBRE, ALU_APELLIDO) values
  (1,1, 'Perengano', 'Perengano');


   -- Crea un bacup diferencial 2
  backup database sae
   to DISK = 'C:\backup\sae_diferencial2.dif'
  WITH INIT, COMPRESSION, DIFFERENTIAL

  -- crea una backup log 

   -- Crea un backup log diferencial/con error
  backup log sae
   to DISK = 'C:\backup\saeLog_diferencial.log'
  WITH INIT, COMPRESSION, DIFFERENTIAL


     -- Crea un backup log 
  backup log sae
   to DISK = 'C:\backup\saeLog.log'
  WITH INIT, COMPRESSION


  -------------------RESTORE -----

  
RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae.bak'
  WITH NORECOVERY

  USE SAE;
  SELECT * FROM ALUMNOS

  -- levanto el primer diferencial

  RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae_diferencial.dif'
  WITH NORECOVERY

  USE SAE;
  SELECT * FROM ALUMNOS

  -- levanto el segundo diferencial

  RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae_diferencial2.dif'
  WITH RECOVERY

  USE SAE;
  SELECT * FROM ALUMNOS

  -- realizar un segundo backup

  BACKUP DATABASE SAE
   TO DISK = 'C:\backup\sae2.bak'
   WITH INIT

   -- realizar un restore con replace 
  RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae.bak'
  WITH REPLACE


  USE SAE;
  SELECT * FROM ALUMNOS


  RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae_diferencial.dif'
  WITH NORECOVERY

   RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae_diferencial.dif'
  WITH RECOVERY

  -- realizar uns respore with replace de la 

  RESTORE DATABASE SAE
  FROM DISK = 'C:\backup\sae2.bak'
  WITH REPLACE

  USE SAE;
  SELECT * FROM ALUMNOS
