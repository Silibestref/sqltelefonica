use DB;
go

CREATE VIEW provinciasPartidos_view
as select pro_descripcion, par_descripcion
from provincias as pro inner join partidos as par
on pro.PRO_ID = par.PRO_ID;
