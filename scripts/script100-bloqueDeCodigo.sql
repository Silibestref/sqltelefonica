use cine;

-- para el if
if (select count(*) 
		from peliculas)>5
		begin
			print 'son mas de 5 peliculas'
			print 'la parte positiva'
			print 'si es mas de una linea debo hacer un bloque'
		end
else 
	   print 'son mas de 5 peliculas'

-- agregar una columna
use cine;
alter table peliculas
	add  PEL_IMBD float NULL; 

alter table peliculas
	drop column PEL_IMBD;

alter table peliculas
  add constraint FK_PEL_GEN FOREIGN KEY(GEN_ID) REFERENCES GENEROS(GEN_ID);
-- hacer un select que se cumple cuando el campo es nulo-----------

select count(*) as 'cantidad de nulos en imdb'
from peliculas
where PEL_IMBD IS NULL 

-- muestra la cantidad de peliculas
select count(*) as CANT_PELICULAS
from peliculas

-- 5-	Realizar un ciclo while de 10 vueltas, para ello definir una variable para ser utilizada como contador y muestre cada una de las vueltas.

use cine;
DECLARE @CONT INT;
SET @CONT = 0;
PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<10)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
	end

-- 6- Tomando como referenncia el mismo ciclo whie agregar un select sobre la tabla peliculas donde establezca un where con cada vuelta del contador ----------
use cine;
DECLARE @CONT INT;
SET @CONT = 0;
PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<10)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);

		SELECT * FROM PELICULAS
		WHERE PEL_ID = @CONT
	end

-- 7 Ampliando el c�digo anterior, ahora definir una variable para tener la cantidad de peliculas y modificar el c�digo para utilizarla-----------

use cine;
DECLARE @CONT INT;
DECLARE @CANT_PEL INT;
-- asignacion on set
SET @CONT = 0;
-- asignacion con select
SELECT @CANT_PEL = COUNT(*) FROM PELICULAS;

PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<@CANT_PEL)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);

		SELECT * FROM PELICULAS
		WHERE PEL_ID = @CONT
	end
-- 8- Modificar el c�digo de deginir una variable donde se guarde el gen_id, y mostrar el gen_id a traves de un print ---------------

use cine;
DECLARE @CONT INT;
DECLARE @CANT_PEL INT;
DECLARE @GEN_ID INT;
-- asignacion on set
SET @CONT = 0;
-- asignacion con select
SELECT @CANT_PEL = COUNT(*) FROM PELICULAS;

PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<@CANT_PEL)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);

		SELECT * 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;

		SELECT @GEN_ID =GEN_ID 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;
		PRINT 'GEN_ID=' + CONVERT(VARCHAR, @GEN_ID);

		SELECT *
		FROM GENEROS
		WHERE GEN_ID = @GEN_ID;
	end

	/* 
9-	 Luego, de acuerdo al g�nero, que guardo en una variable @GEN_DESC, escribo los siguientes textos, realizando la comparaci�n en may�sculas (upper) pag 170/1347.
	a.		 La comedia me hace muy feliz.
	b.		 La acci�n tiene mucho movimiento.
	c.		El drama me hace llorar.
	d.		El terror me da mucho miedo.


	*/

use cine;
DECLARE @CONT INT;
DECLARE @CANT_PEL INT;
DECLARE @GEN_ID INT;
DECLARE @GEN_DESC VARCHAR(50);
-- asignacion on set
SET @CONT = 0;
-- asignacion con select
SELECT @CANT_PEL = COUNT(*) FROM PELICULAS;

PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<@CANT_PEL)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);

		SELECT * 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;
		-- asigno variables
		SELECT @GEN_ID =GEN_ID 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;
		PRINT 'GEN_ID=' + CONVERT(VARCHAR, @GEN_ID);
		
		SELECT @GEN_DESC = GEN_DESCRIPTION 
		FROM generos
		WHERE GEN_ID = @GEN_ID;
		PRINT 'GEN_DESC=' + upper( @GEN_DESC);
		IF upper( @GEN_DESC)='COMEDIA'
			print 'la comedia me hace muy feliz'

		else if upper( @GEN_DESC)='ACCION'
			print 'la accion tiene mucho movimiento'

		else if upper( @GEN_DESC)='DRAMA'
			print 'el drama me hace llorar'

		else if upper( @GEN_DESC)='TERROR'
			print 'el terror me da mucho miedo'

		SELECT *
		FROM GENEROS
		WHERE GEN_ID = @GEN_ID;
	end
/*
10-	 A continuaci�n, asigno el valor imdb de acuerdo al g�nero y la siguiente tabla
a.	comedia 4.5
b.	accion 6.8
c.	drama 5.3
d.	terror 5.7 
*/

use cine;
DECLARE @CONT INT;
DECLARE @CANT_PEL INT;
DECLARE @GEN_ID INT;
DECLARE @GEN_DESC VARCHAR(50);
-- asignacion on set
SET @CONT = 0;
-- asignacion con select
SELECT @CANT_PEL = COUNT(*) FROM PELICULAS;

PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);
WHILE (@CONT<@CANT_PEL)
	begin
		SET @CONT=@CONT +1;
		PRINT 'CONT=' + CONVERT(VARCHAR, @CONT);

		SELECT * 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;
		-- asigno variables
		SELECT @GEN_ID =GEN_ID 
		FROM PELICULAS
		WHERE PEL_ID = @CONT;
		PRINT 'GEN_ID=' + CONVERT(VARCHAR, @GEN_ID);
		
		SELECT @GEN_DESC = GEN_DESCRIPTION 
		FROM generos
		WHERE GEN_ID = @GEN_ID;
		PRINT 'GEN_DESC=' + upper( @GEN_DESC);
		IF upper( @GEN_DESC)='COMEDIA'
			begin
			print 'la comedia me hace muy feliz';
			
			update peliculas 
			set PEL_IMBD = 4.5
			WHERE PEL_ID = @CONT;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @CONT;

			end

		else if upper( @GEN_DESC)='ACCION'
			begin
			print 'la accion tiene mucho movimiento'

			update peliculas 
			set PEL_IMBD = 6.8
			WHERE PEL_ID = @CONT;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @CONT;

			end

		else if upper( @GEN_DESC)='DRAMA'
			begin
			print 'el drama me hace llorar'

			update peliculas 
			set PEL_IMBD = 5.3
			WHERE PEL_ID = @CONT;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @CONT;

			end

		else if upper( @GEN_DESC)='TERROR'
			begin
			print 'el terror me da mucho miedo'

			update peliculas 
			set PEL_IMBD = 5.7
			WHERE PEL_ID = @CONT;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @CONT;

			end

		SELECT *
		FROM GENEROS
		WHERE GEN_ID = @GEN_ID;
	end
	

	/*
	11-	Ahora asignar null a todos los pel_imd 
	*/
	use cine;
	update peliculas
	set PEL_IMBD = null;


/*
12-	Realizar un inner join en donde se obtenga pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPTION	

*/
use cine;
select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPTION
from peliculas as pel inner join generos as gen
on pel.GEN_ID= gen.GEN_ID


/*
13-	Realizar un cursor y recorrerlo de manera de guardar cada uno de los campos 
pel_id, 
pel_descripcion,  
gen.gen_id, 
GEN_DESCRIPTION 
en variables y sean mostrados a partir de un print
*/

use cine;
DECLARE @PEL_ID INT;
DECLARE @PEL_DESC VARCHAR(50);
DECLARE @GEN_ID INT;
DECLARE @GEN_DESC VARCHAR(50);
DECLARE @P_CURSOR AS CURSOR; -- defino el cursor
--defino el cursor

SET @P_CURSOR = CURSOR FOR
	select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPTION
	from peliculas as pel inner join generos as gen
	on pel.GEN_ID= gen.GEN_ID;

-- abro el cursor
OPEN @P_CURSOR;
--
FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC
WHILE @@FETCH_STATUS=0
BEGIN
	PRINT 'PEL_ID=' + CONVERT(VARCHAR,@PEL_ID);
	PRINT 'PEL_DESC=' + CONVERT(VARCHAR,@PEL_DESC);
	PRINT 'GEN_ID=' + CONVERT(VARCHAR,@GEN_ID);
	PRINT 'GEN_DESC=' + CONVERT(VARCHAR,@GEN_DESC);
	PRINT '------------'

	FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC;
END
CLOSE @P_CURSOR;
DEALLOCATE @P_CURSOR;

/*
14-	Utilizar ele ejercicio anterior agregando  los  updates modificando los imdb seg�n lo  mostrados en el punto 10.
*/

use cine;
DECLARE @PEL_ID INT;
DECLARE @PEL_DESC VARCHAR(50);
DECLARE @GEN_ID INT;
DECLARE @GEN_DESC VARCHAR(50);
DECLARE @P_CURSOR AS CURSOR; -- defino el cursor
--defino el cursor

SET @P_CURSOR = CURSOR FOR
	select pel_id, pel_descripcion,  gen.gen_id, GEN_DESCRIPTION
	from peliculas as pel inner join generos as gen
	on pel.GEN_ID= gen.GEN_ID;

-- abro el cursor
OPEN @P_CURSOR;
--
FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC
WHILE @@FETCH_STATUS=0
BEGIN
	PRINT 'PEL_ID=' + CONVERT(VARCHAR,@PEL_ID);
	PRINT 'PEL_DESC=' + CONVERT(VARCHAR,@PEL_DESC);
	PRINT 'GEN_ID=' + CONVERT(VARCHAR,@GEN_ID);
	PRINT 'GEN_DESC=' + CONVERT(VARCHAR,@GEN_DESC);
	PRINT '------------'
-- esto es copiado del ejercicio 10 ---------
		IF upper( @GEN_DESC)='COMEDIA'
			begin
			print 'la comedia me hace muy feliz';
			
			update peliculas 
			set PEL_IMBD = 4.5
			WHERE PEL_ID = @PEL_ID;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @PEL_ID;

			end

		else if upper( @GEN_DESC)='ACCION'
			begin
			print 'la accion tiene mucho movimiento'

			update peliculas 
			set PEL_IMBD = 6.8
			WHERE PEL_ID = @PEL_ID;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @PEL_ID;

			end

		else if upper( @GEN_DESC)='DRAMA'
			begin
			print 'el drama me hace llorar'

			update peliculas 
			set PEL_IMBD = 5.3
			WHERE PEL_ID = @PEL_ID;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @PEL_ID;

			end

		else if upper( @GEN_DESC)='TERROR'
			begin
			print 'el terror me da mucho miedo'

			update peliculas 
			set PEL_IMBD = 5.7
			WHERE PEL_ID = @PEL_ID;


			SELECT * 
			FROM PELICULAS
			WHERE PEL_ID = @PEL_ID;

			end


	FETCH NEXT FROM @P_CURSOR INTO @PEL_ID, @PEL_DESC, @GEN_ID, @GEN_DESC;
END
CLOSE @P_CURSOR;
DEALLOCATE @P_CURSOR;


/*
15-	Realizar un ingreso  el ingreso de los g�neros  
a.	Ciencia ficci�n
b.	Documental
c.	Policial
Luego de ello insertar una pel�cula con �Esperando la carroza� con el gen_id = 50, todo esto debe ser realizado dentro de una transacci�n  y con una estructura try Catch, y, de ocurrir un error, realizar un rollback

*/

use cine;
BEGIN TRY
	BEGIN TRANSACTION
		insert into generos values('Ciencia Ficci�n');
		insert into generos values('Documental');
		insert into generos values('Policial');
		insert into peliculas values(50, 'Esperando la Carroza', 9);
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	ROLLBACK;
	PRINT 'Ocurrio un error';
	THROW
END CATCH

/*
16-	Realizar un select * g�neros y ver el resultado
*/
select * from generos

/*

17-	Realizar un stored procedure llamado sp_modificarImdb que primita ingresar la pel_id y el puntaje de imdb, 
luego con esos datos, modificar la tabla pel�cula con los datos ingresados
*/

use cine;
drop PROCEDURE sp_modificarImdb;

use cine;
go
CREATE PROCEDURE sp_modificarImdb(@PEL_ID INT, @PEL_IMBD FLOAT )
	AS
		update peliculas
		set PEL_IMBD = @PEL_IMBD
		where PEL_ID = @PEL_ID;					


/*
18-	Ejecutar el sp anterior asignan puntaje a  5 las  pel�culas, modificando el imdb
*/




/*
18-	Ejecutar el sp anterior asignan puntaje a  7.6 a  la pel�cula �la pistola desnuda� 
*/

use cine;

execute sp_modificarImdb 1,7.6;

/*
19-	Verificar a trav�s de un select la modificaci�n realizada
*/
use cine;
select * from peliculas;

/*
20-	Modificar el sp de manera de manera de verificar que el par�metro de imdb se encuentre 
entre 0 y 10 y de no darse esta condici�n se deber� informar el 
texto 'los valores de imdb deben deben tener valores entre 0 y 10'
*/
use cine;
drop PROCEDURE sp_modificarImdb;


use cine;
go
CREATE PROCEDURE sp_modificarImdb(@PEL_ID INT, @PEL_IMBD FLOAT )
	AS
		if @PEL_IMBD >0 and @PEL_IMBD  <11
				update peliculas
				set PEL_IMBD = @PEL_IMBD
				where PEL_ID = @PEL_ID;					
		else 
			print 'los valores de imdb deben deben tener valores entre 0 y 10'


/*
21-	Correr el sp de manera de realizar las siguientes siguientes asignaciones
 la pistola desnuda 7.6
 gladirador 8.5
 Titanic 7.9
 Rambito y Rambon 5.1
 la llamda 7.1
 la profecia 7.5
 los ba�eros mas locos del mjndo 6.5
 el resplandor 8.4
 batman 7.9
 miaion imposible 7.9
 encuentro explosiva 6.3
 el sr y la sra smith 6.5
 troya 3.9
 la isla siniestra 8.2

*/

use cine;
sp_modificarImdb 1, 7.6;
sp_modificarImdb 2, 8.5;
sp_modificarImdb 3, 7.9;
sp_modificarImdb 4, 5.1;
sp_modificarImdb 5, 7.1;
sp_modificarImdb 6, 7.5;
sp_modificarImdb 7, 6.5;
sp_modificarImdb 8, 8.4;
sp_modificarImdb 9, 7.9;
sp_modificarImdb 10, 7.9;
sp_modificarImdb 11, 6.3;
sp_modificarImdb 12, 6.5;
sp_modificarImdb 13, 3.9;
sp_modificarImdb 14, 8.2;

/*
22- Llamar a l mismo sp en, pero en esta oportunidad enviando un 11 como  imdb y -3, copiar los mensajes recibidos.
*/

sp_modificarImdb 14, 11;
sp_modificarImdb 14, -3;

/*
23- Realizar un sp de nominado sp_agregarPelicula, el cual tenga los siguientes par�metros.
	a. gen_id
	b. pel_desc
	c. pel_imdb
Se deber� validar el  imdb teniendo en cuanta que el rango debe ser entre 0 y 10 para el imdb  informando el error 'los valores de imdb deben deben tener valores entre 0 y 10'.
Tambi�n se debe agregar una estructura try catch en donde informe en catch el error 'Se introdujo un genero inexistente'

*/

use cine;
drop procedure agregarPelicula

use cine;
go
CREATE PROCEDURE agregarPelicula(@GEN_ID INT, @PEL_DESC VARCHAR(50), @PEL_IMBD FLOAT )
	AS
		if @PEL_IMBD >0 and @PEL_IMBD  <11
				BEGIN TRY
					insert into peliculas(GEN_ID, PEL_DESCRIPCION, PEL_IMBD) 
					values (@GEN_ID, @PEL_DESC, @PEL_IMBD);
			
				END TRY

				BEGIN CATCH
					THROW
					print 'Se introdujo un genero inexistente'
					
				END CATCH
		else 
			print 'los valores de imdb deben deben tener valores entre 0 y 10'


/*
24- Ejecutar el stored procedure agregando las siguientes pel�culas
	a. Esperando la carroza 6.5 de imdb, comedia
	b. Top gun -6.9 de imdb, accion
	c. jurasic park 8.2 de imdb, accioin
*/

use cine;
exec agregarPelicula  5, 'Esperando la carroza', 6.5
exec agregarPelicula  3, 'Top gun', 6.9
exec agregarPelicula  3, 'Jurasik Parck', 8.2

/*
25- Llamar al sp y probar con un imbd err�neo y con una gen_id err�neo.
a. imdb 12
b. gen_id = 50
*/
use cine;
exec agregarPelicula  5, 'Esperando la carroza', 12
exec agregarPelicula  33, 'Top gun', 6.9


/*
26-  Realizar un sp de nominado sp_modificarPelicula, el cual tenga los siguientes par�metros.
	a. pel_id
b. gen_id
	c. pel_desc
	d. pel_imdb
Tener en cuenta que pel_id debe existir para lo cual se utilizar� una variable que 
Se deber� validar el  imdb teniendo en cuanta que el rango debe ser entre 0 y 10 para el imdb  informando el error 'los valores de imdb deben deben tener valores entre 0 y 10'.
Tambi�n se debe agregar una estructura try catch en donde informe en catch el error 'Se introdujo un genero inexistente'

*/

use cine;
go
CREATE PROCEDURE modificarPelicula(@PEL_ID INT, @GEN_ID INT, @PEL_DESC VARCHAR(50), @PEL_IMBD FLOAT )
	AS
		DECLARE @DESC_LEIDA VARCHAR(50);
		
		SELECT @DESC_LEIDA = PEL_DESCRIPCION
		FROM peliculas
		WHERE PEL_ID = 33;

		print '*' + @DESC_LEIDA  + '*';
	

		if @PEL_IMBD >0 and @PEL_IMBD  <11
				BEGIN TRY
					update peliculas
					set GEN_ID			=	@GEN_ID		, 
						PEL_DESCRIPCION	=	@PEL_DESC	, 
						PEL_IMBD		=	@PEL_IMBD
					where PEL_ID = @PEL_ID
					  
			
				END TRY

				BEGIN CATCH
					THROW
					print 'Se introdujo un genero inexistente'
					
				END CATCH
		else 
			print 'los valores de imdb deben deben tener valores entre 0 y 10'


/*
27- Modificar la pistola desnuda colocando el texto en may�scula
*/
use cine;
exec modificarPelicula  1,5,'LA PISTOLA DESNUDA', 7.6

/*
28- Realizar un sp llamado sp_getPeliculasByGenero, 
en donde recibe por par�metro el genero y devuelve una 
lista de las pel�culas indiando la descripci�n del genero  con los 
campos GEN_DESCRIPTION, PEL_ID, PEL_DESCRIPCION, PEL_IMBD que pertenecen a ese g�nero, 
para lo cual se deber� realizar un inner join
*/

use cine;
drop procedure sp_getPeliculasByGenero

use cine;
go
CREATE PROCEDURE sp_getPeliculasByGenero(@GEN_ID INT)
	AS
	select GEN_DESCRIPTION ,PEL_ID, PEL_DESCRIPCION, PEL_IMBD
	from peliculas as pel inner join generos as gen
		on pel.GEN_ID = gen.GEN_ID
	where pel.GEN_ID = @GEN_ID

/*
29- Ejecutar el sp para los g�neros disponibles
*/
use cine;

exec sp_getPeliculasByGenero 1
exec sp_getPeliculasByGenero 2
exec sp_getPeliculasByGenero 3
/*
30- Utilizar la funci�n with result sets para modificar los encabezados del resultado 
de la consulta cambiando los nombres 
a  [Genero] varchar(50),[Codigo] int,[nombre de pel�cula] varchar(50),[puntaje] float.
*/

use cine;
exec sp_getPeliculasByGenero 3
with result sets
	(([Genero] varchar(50),
	[Codigo] int,
	[nombre de pel�cula] varchar(50),
	[puntaje] float
	))

/*
31-	Crear una funci�n denominada fn_getCantidadDePeliculasPorGenero que recibe como par�metro un numero de genero
*/
use cine;
go
create function getCantidadDePeliculasPorGenero(@gen_id int) 
	returns int

	begin
		DECLARE @Cantidad int

		select @Cantidad = count(*)
		from peliculas
		where GEN_ID = @gen_id

				
		return @Cantidad;
	end

	/*
	32- Probar que funciona llam�ndola para cada uno de los g�neros.
	*/
	use cine;
	print 'cantidad de peliculas de 1 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(1) );
	print 'cantidad de peliculas de 3 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(2) );
	print 'cantidad de peliculas de 3 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(3) );
	print 'cantidad de peliculas de 4 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(4) );
	print 'cantidad de peliculas de 5 son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(5) );

/*
33- Realizar una funci�n denominada fn_getDescripcionDeGenero que reciba el gen_id y devuelva la descripci�n
*/
use cine;
drop function fn_getDescripcionDeGenero;

use cine;
go
create function fn_getDescripcionDeGenero(@gen_id int) 
	returns varchar(50)

	begin
		DECLARE @descripcion varchar(50);

		select @descripcion = GEN_DESCRIPTION
		from generos
		where GEN_ID = @gen_id

				
		return @descripcion;
	end

	/*
	34- Probar llam�ndola a cada uno de los par�metros y al mismo tiempo llamar a  fn_getCantidadDePeliculasPorGenero y , de esta manera dar la informaci�n completa
	*/
	use cine;
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(1) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(1) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(2) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(2) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(3) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(3) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(4) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(4) );
	print 'cantidad de peliculas de ' + dbo.fn_getDescripcionDeGenero(5) + ' son ' + convert(varchar, dbo.getCantidadDePeliculasPorGenero(5) );

	/*
	35- Crear una funci�n que devuelva una todas las pel�culas que tengan un g�nero especificado
	*/

	use cine;
	go
	create function fn_getPeliculasPorGenero(@gen_id int)
		returns table 
		
		as

		return (select PEL_ID, PEL_DESCRIPCION
		from peliculas	
		where GEN_ID= @gen_id);

/*
36- probar que la misma funciona correctamente utilizando la sentencia select * from
*/

use cine;
print 'genero ' + dbo.fn_getDescripcionDeGenero(1) ;
select * from fn_getPeliculasPorGenero(1);
select * from fn_getPeliculasPorGenero(2);
select * from fn_getPeliculasPorGenero(3);
select * from fn_getPeliculasPorGenero(4);

/*
37- Agregar a la tabla de g�neros, Aventura, Ciencia Ficcion y Policial
*/

use cine 
insert into generos values ('Aventura'),
							('Ciencia Ficcion'),
							('Policial');


-- hacemos un inner join
/*
38- Realizar n inner join que permita mostrar los 
campos GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION de las tablas g�neros y pel�culas.
*/
use cine;
select GEN.GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION
from generos as gen inner join peliculas as pel
on gen.GEN_ID = pel.GEN_ID


/*
39- Realizar la opci�n cross aply y utilizar la funci�n fn_getPeliculasPorGenero en lugar de la tabla pel�culas 
*/
use cine;
select GEN.GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION
from generos as gen
cross apply dbo.fn_getPeliculasPorGenero(GEN.GEN_ID) as pel



-- hacemos un outer join
/*
40- Realizar n inner join que permita mostrar los campos GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION de las tablas g�neros y pel�culas.
*/
use cine;
select GEN.GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION
from generos as gen left join peliculas as pel
on gen.GEN_ID = pel.GEN_ID

/*
41-  Realizar la opci�n aouter aply y utilizar la funci�n fn_getPeliculasPorGenero en lugar de la tabla pel�culas 
*/
select GEN.GEN_ID,GEN_DESCRIPTION, PEL_DESCRIPCION
from generos as gen
outer apply dbo.fn_getPeliculasPorGenero(GEN.GEN_ID) as pel

